import arcpy
from arcpy import env
from arcpy.sa import *

import os
import shutil
#from osgeo import gdal
import numpy as np
import pandas as pd


#set arcpy global variables
arcpy.CheckOutExtension("Spatial") #get spatial analyst extention
arcpy.env.overwriteOutput = True
#arcpy.env.scratchFolder = "C:/Users/schmidle/Desktop/Scratch"

# filepaths
#office
#path = "C:/Users/schmidle/Desktop/Masterarbeit/RawData/morphdata/"#Input
#path2 = "C:/Users/schmidle/Desktop/Masterarbeit/Analysis/Data/Geodata/Predictors/"#Output
#server
path = "C:/Users/schmidle/Desktop/morphdata/"#Input
path2 = "C:/Users/schmidle/Desktop/Predictors/"#Output


# lists for output
Station = []
StrahlerMax = []
Area = []
LengthAll = []
DD = []
ChLength = []
ChSlope = []
Slope = []
FLSD = []
FLCV = []
FLMax = []
Errors = ["WARNINGS: "]
direct =  filter(lambda x: os.path.isdir(os.path.join(path, x)), os.listdir(path))#folder list for
#iteration, isdir and .join to exlude non-folders
count =1
for i in direct:
    catch_id = i.translate(None, 'sub_')#catchment ID
    Station.append(catch_id)#for output table
    print("BASIN: "+catch_id+" , "+str(count)+" of "+str(len(direct)))
    path_in = path+i+"/" #paths for each subfolder per basin
    print("IN: "+str(path_in))
    arcpy.env.workspace = path_in
    path_out = path2+i+"/"
    print("OUT: "+str(path_out))
    if not os.path.exists(path_out):#create subfolders
        os.makedirs(path_out)
        
    #clear subfolder to avoid problems with arcpy overwriting that doesnt always work
    for name in os.listdir(path_out):
        p = os.path.join(path_out, name)
        try:
            if os.path.isdir(p):
                shutil.rmtree(p)
            else:
                os.remove(p)
        except shutil.Error as e:
            print ('Ein Fehler ist aufgetreten: ', e)

    #input rasters
    facc = "facc.asc"
    fdir = "fdir.asc"


    # set projection (once is enough)
##    sr = arcpy.SpatialReference(3397)
##    arcpy.DefineProjection_management(facc, sr)
##    arcpy.DefineProjection_management(fdir, sr)
    
    ##### create polygon of catchment #####     
    outCatch =Con(facc, catch_id, "", "Value >= 0")#set all non-null vlaues to catchment ID
    #pathout1 = path_out+"facc_mask.asc"
    #arcpy.RasterToASCII_conversion(outCatch, pathout1)#save mask raster
    pathout2 = path_out+"CatchPoly.shp"
    arcpy.RasterToPolygon_conversion(outCatch, pathout2, "SIMPLIFY", "VALUE","MULTIPLE_OUTER_PART")

    outCatch = None #clear in-memory rasters to speed up computation

    ##### extract stream network #####
    streams =Con(facc, 1, "", "Value > 100")#all flow acc values greater 100 = stream
    streamorder = StreamOrder(streams, fdir, "STRAHLER")
    streamorder_arr = arcpy.RasterToNumPyArray(streamorder,nodata_to_value=-9999)# get unique stream order values,set noData to -9999
    values = np.unique(streamorder_arr)
    maxvals = sorted(values)[-3:]#limit to three highest stream orders
    print(maxvals)
    count_max = np.count_nonzero(streamorder_arr == (max(maxvals)))#calculate n pixel of mainstream
    print("Main Stream n Pixel: "+str(count_max))
    #if <5 there might be 2 large streams instead of one main stream--> have to be recalculated manually
    if (count_max < 5):
        maxvals[:] = [x - 1 for x in maxvals]
        print("SMALLER 5, CORRECTED TO: "+str(maxvals))
        Errors.append(catch_id+": 2 streams?")        
    StrahlerMax.append(max(maxvals))#write highest order to output
    wherearg = "Value >= "+str(maxvals[0])
    streamorder_cut = Con(streamorder,streamorder,"",wherearg)
    pathout3 = path_out+"StreamOrder.asc"
    arcpy.RasterToASCII_conversion(streamorder, pathout3)#save streamorder raster
    pathout4 = path_out+"StreamOrder_cut.shp"
    arcpy.RasterToPolyline_conversion(streamorder_cut, pathout4, background_value="NODATA", simplify="SIMPLIFY")#save reduced streamorder features

    streams = None

    ####### Calculate Drainage Density #####
    streamorder_poly = arcpy.RasterToPolyline_conversion(streamorder_cut, r"in_memory\polyline", background_value="NODATA", simplify="SIMPLIFY")
    length_streams = sum([f[0] for f in arcpy.da.SearchCursor(streamorder_poly, 'SHAPE@LENGTH')])/1000# stream length in km
    area_catchment = [f[0] for f in arcpy.da.SearchCursor(pathout2, 'SHAPE@AREA')]
    area_catchment = area_catchment[0]/(10**6)#in km2
    Area.append(area_catchment)
    print("length_streams:"+str(length_streams))
    print("area_catchment:"+str(area_catchment))
    LengthAll.append(length_streams)
    DD.append(length_streams/area_catchment)#output

    streamorder_cut = None
    streamorder_poly = None

    ##### Calculate Main Channel Length #####
    wherearg2 = "Value = "+str(max(maxvals))
    mainstream = Con(streamorder,streamorder,"",wherearg2)
    #pathout88 = path_out+"mainstream.asc"
    #arcpy.RasterToASCII_conversion(mainstream, pathout88)
    mainstream_poly = arcpy.RasterToPolyline_conversion(mainstream, r"in_memory\polyline", background_value="NODATA", simplify="SIMPLIFY")
    length_mainstream = sum([f[0] for f in arcpy.da.SearchCursor(mainstream_poly, 'SHAPE@LENGTH')])/1000# main stream length in km
    ChLength.append(length_mainstream)#output

    streamorder_arr = None
    streamorder = None
    facc = None
    mainstream_poly = None

    ##### Calculate Main Channel Slope #####
    slope = "slope.asc"
    #arcpy.DefineProjection_management(slope, sr)
    slope_mainstream = ExtractByMask (slope, mainstream)
#    slope_mainstream_m =  arcpy.GetRasterProperties_management(slope_mainstream, "MEAN").getOutput(0).replace(",", ".")#replace decimal symbol
    slope_mainstream_m = slope_mainstream.mean
    ChSlope.append(slope_mainstream_m)

    mainstream = None
    slope_mainstream = None


    ##### Calculate Catchment Slope #####
    slope = arcpy.CalculateStatistics_management(slope)
    slope_catchment = arcpy.GetRasterProperties_management(slope, "MEAN").getOutput(0).replace(",", ".")
    Slope.append(slope_catchment)

    slope = None

    ##### Calculate SD, CV and Max of Flow Distance #####
    flowlength = FlowLength(fdir, "DOWNSTREAM")#calculate flow length
    flowlength_std = flowlength.standardDeviation/1000#in km
##    flowlength_std = arcpy.GetRasterProperties_management(flowlength, "STD").getOutput(0).replace(",", ".")#sd
##    flowlength_std = float(flowlength_std)/1000
    FLSD.append(flowlength_std)
##    flowlength_mean = arcpy.GetRasterProperties_management(flowlength, "MEAN").getOutput(0).replace(",", ".")#mean
##    flowlength_mean = float(flowlength_mean)/1000#in km
    flowlength_mean = flowlength.mean/1000#in km    
    FLCV.append(flowlength_std/flowlength_mean)
    print("flowlength_std:"+str(flowlength_std))
    print("flowlength_CV:"+str(flowlength_std/flowlength_mean))
    #pathout5 = path_out+"Flowlength.asc"
    #arcpy.RasterToASCII_conversion(flowlength, pathout5)
    flowlength_max = flowlength.maximum/1000
##    flowlength_max = arcpy.GetRasterProperties_management(flowlength, "MAXIMUM").getOutput(0).replace(",", ".")#max
##    flowlength_max = float(flowlength_max)/1000#in km
    FLMax.append(flowlength_max)
    flowlength = None
    fdir = None


    count = count+1
    #clean all variables/raster layers (otherwise arcpy scratch rasters persist)
    #path_in = None
    #path_out = None
    #pathout2 = None
    #slope_mainstream_m = None
    #slope_catchment = None



    
# Create Output table
output = pd.DataFrame(np.column_stack([Station, Slope, StrahlerMax, LengthAll, Area, DD, ChLength, ChSlope, FLSD, FLCV, FLMax]),
                      columns=["Station", "Slope", "StrahlerMax", "LengthAll", "Area", "DD", "ChLength", "ChSlope", "FLSD", "FLCV", "FLMax"])
#print(output)
output.to_csv(path2+"Geoprocess_Preds.csv",index=False, sep=";")
print(Errors)
#output.to_csv("C:/Users/schmidle/Desktop/Geoprocess_Preds.csv",index=False, sep=";")
# Check out the ArcGIS Spatial Analyst extension license SUM_<field>
arcpy.CheckOutExtension("Spatial") 
