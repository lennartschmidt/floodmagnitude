library(dplyr)
###RETRANSFOMRATION OF GLM COEFFICIENTS FROM PCA
#pdf(paste0(pathanal,"/0 All/8_Coefficients_GLM2.pdf"),width=20,height=10) 
Coeffs <- list()
for(i in 1:4){
  fm1 <- readRDS(paste0(pathanal,"/",i,"/","fmglm2_r_S","_Reg",i,".R"))
  rotmat1 <- readRDS(paste0(pathanal,"/",i,"/","rot_fmglm2_r_S","_Reg",i,".R"))$rotation
  fm2 <- readRDS(paste0(pathanal,"/",i,"/","fmglm2_r_W","_Reg",i,".R"))
  rotmat2 <- readRDS(paste0(pathanal,"/",i,"/","rot_fmglm2_r_W","_Reg",i,".R"))$rotation
  labs <- as.list("GLM2_PCAraw","GLM2_PCAraw")
  names <- as.list(paste0(c("S","W"),i))
  out <- mapply(FUN=funcRetransform,fm=list(fm1,fm2),rotmat=list(rotmat1,rotmat2),name=names,label=labs)
  par(mfrow=c(2,1))
  #barplot(out[[1]],las=2,main=paste0(names[1],": ",labs[1]))
  #barplot(out[[2]],las=2,main=paste0(names[2],": ",labs[1]))  
  Coeffs <- append(Coeffs,out)
}
#dev.off()  
saveRDS(Coeffs,paste0(pathanaldat,"/7_Coeffs_",labs[1],".R"))  

###Sum UP 1st and 2nd POLYNOM FOR VISUALIZATION
# turn coefficents-list into dataframe
a <- lapply(Coeffs, as.data.frame)
Coeffs <- readRDS(paste0(pathanaldat,"/7_Coeffs_",labs[1],".R"))
Coeffs_df <- data.frame(Var=names(Coeffs[[1]]))
for (i in 1:length(Coeffs)){#iterate through regions
  sub <- Coeffs[[i]]
  sub <- data.frame(names(sub),as.numeric(sub))#turn list element into dataframe
  names(sub) <- c("Var", names(Coeffs)[i])
  Coeffs_df <- merge(Coeffs_df,sub)
}  
###summing and into one dataframe
#get normal names
inp <- read.csv(paste0(pathanaldat,"/NON STANDARDIZED/5_Reg",1,"_Tr_S.csv"))#get single predictor names
dropvar <- which(names(inp)%in%c("Station","Qmax","Qmean","Qvol","Area_GIS","Station1","EventID","Event","YYYY","Startdate","Enddate","Duration","Group_ID","Keep","Region","Subregion",
                                 "StrahlerMax","LengthAll","FLSD","PET_Ann","Peff_Ann","Elev_SD","Altitude","Elev_Min","Elev_Max","PmaxT","PminT","PeffmaxT","PeffminT","SMmaxT","SMminT","TmaxT","TminT","P0","P1","P3","P5",
                                 "P7","Pmin","Pmax","PET0", "PET1", "PET3", "PET5","PET7",paste0("AET",c(0,1,3,5,7))))
names <- names(inp[,-dropvar])
out <- data.frame(Var=names)#create output dataframe
out[,names(Coeffs_df)[-1]] <- 0
for (i in 1:length(names)){#fill dataframe with sums
  index <- which(Coeffs_df$Var%in%c(names[i],paste0(names[i],"^2")))
  out[i,-1] <- apply(Coeffs_df[index,-1],2,sum)#1=Var#summing up is possible as all values were scaled to [0,1] before
}
out
saveRDS(out,paste0(pathanaldat,"/7_Coeffs_Sums_",labs[1],".R")) 
######################################################################
###FUNCTIONS 
#############################################################################

funcRetransform <- function(fm,rotmat,name,label){
  # rotmat<- apply(rotmat1,2,FUN=function(x){abs(x)/max(abs(x))})#standardize to max value??
  # rotmat<- abs(rotmat)#absolute values to see contrivution
  coeffs <- fm$coefficients
  #get rotation matrix and varimp to have same variables and in the same order
  coeffs1 <- coeffs[-which(names(coeffs)=="(Intercept)")]#EXCLUDE INTERCEPT as there is n need to transform it
  ###merge square PCs and PCs
  coeffdf <- data.frame(matrix())
  
  
  a <- order(names(coeffs1))#order coeffs 
  keep <- which(colnames(rotmat)%in%names(coeffs1))##if PCs were dropped by stepAIC
  rotmat_ordered <- rotmat[,keep]#remove the ones that were dropped in model selection
  rotmat_ordered <- rotmat_ordered[,order(colnames(rotmat_ordered))]#order
  coeffs2 <- t(as.matrix(coeffs1[a]))#transform ordered coefficients
  print(paste0("Same order? ",identical(colnames(coeffs2),colnames(rotmat_ordered))))#identical?
  #retransform
  coeffs2 %*% t(rotmat_ordered) -> coeffs_out#retransformation of rotmat is needed for matrix multiplication (verified the code by transforming and retransforming code. see 117_tryPCA.R)
  coeffs_out2 <- list(coeffs_out[,order(coeffs_out[1,],decreasing=T)])#order coeffs by value
  names(coeffs_out2) <- name #name the list for list of all 8 models
  return(coeffs_out2)
}



