#########################################
###Merge two different runs of hyperparametertuning
#########################################
funcMergeTun <- function(region,name1,name2,nameout){
  gridout1 <-  read.csv(paste0(pathanal,"/",region,"/",name1,"_HTgrid_Reg",region,".csv"))
  gridout2 <-  read.csv(paste0(pathanal,"/",region,"/",name2,"_HTgrid_Reg",region,".csv"))
  gridoutout <- rbind(gridout1,gridout2)
  write.csv(gridoutout,paste0(pathanal,"/",region,"/",nameout,"_HTgrid_Reg",region,".csv"))
}

#########################################
###3D Plots of Hyperparameter-Tuning
#########################################

func3D <- function (name,region,metric,indxno=1){
  gridout <-  read.csv(paste0(pathanal,"/",region,"/",name,"_HTgrid_Reg",region,".csv"))
  gridout$lr <- as.factor(paste0("lr=",gridout$lr))
  gridout$momentum <- as.factor(paste0("m=",gridout$momentum))
  gridout$lm <- paste(gridout$lr,gridout$momentum,sep=", ")
  gridout$complexity <-  with(gridout,(39*layer1)+(layer1*layer2)+(layer2*layer3)+layer3+layer1+layer2+layer3)
  gridout %>% arrange(RMSE_te) -> gridout_sort
  
  if (grepl("Rsq",metric)){index <- which(gridout[,metric]>max(gridout[,metric])-0.01*max(gridout[,metric]))}#but a buffer of 1% so that almost-as-good models o
  if (grepl("RMSE",metric)){index <- which(gridout[,metric]<min(gridout[,metric])+0.01*min(gridout[,metric]))}#f lower complexity are preferred
  
  if (length(index)==1&indxno==2){#if only single best model, determine second best
    if (grepl("Rsq",metric)){
      index_best <- order(gridout[,metric],decreasing=T)[2]
      print(paste0("Only one top model, the one of second highest ",metric," was selected"))
    }
    if (grepl("RMSE",metric)){
      index_best <- order(gridout[,metric],decreasing=F)[2]
      print(paste0("Only one top model, the one of second lowest ",metric," was selected"))
    }
  }
  if (length(index)>1&indxno==1){# find the simplest model of of multiple best
    index_comp <- which.min(gridout$complexity[index])#of best, find lowest in complexity
    index_best <- index[index_comp]
    print(paste0("Multiple top models, the one of lowest complexity was selected"))
  }
  if (length(index)>1&indxno==2){# find the second simplest model of of multiple best
    index_comp <- order(gridout$complexity[index])[2]#order all, find second lowest in complexity of these
    index_best <- index[index_comp]
    print(paste0("Multiple top models, the one of second lowest complexity was selected"))
  }
  if (length(index)==1&indxno==1){index_best <- index}
  
  structure <- with(gridout[index_best,],paste0("39-",layer1,"-",layer2,"-",layer3," (",lm,")"))
  
    
  xrot=-11#label rotations
  yrot=67
  zrot=97
  strptxt=0.8#size of facet strips
  dist=c(1,1,1.3)#distance of axis labels to axis
  vp = viewport(height=unit(16, "cm"),width=unit(14, "cm"))#size of plots
  
 
  col.l <- colorRampPalette(c("green", 'yellow',"red"))(100)
  
  #CUSTOM STRIP COLORS  
  bgColors <- c("azure", "red", "blue", "darkblue")
  
  # Create a function to be passed to "strip=" argument of xyplot
  myStripStyle <- function(which.panel, factor.levels, ...) {#custom panel labels
    panel.rect(0, 0, 1, 1,
               col = bgColors[which.panel],
               border = 1)
    panel.text(x = 0.5, y = 0.5,
               font=1,
               lab = factor.levels[which.panel],
               col = "black")#txtColors[which.panel],,UNTEN:strip=myStripStyle
  }

  a <- wireframe(get(metric) ~ (layer1+layer2) * layer3 | lm, data=gridout,xlab=list("layer1+2",rot=xrot),ylab=list("layer3",rot=yrot),zlab=list(metric,rot=zrot),
                 scales = list( arrows = F,distance=dist),#,xlab=list(cex=cxtl),ylab=list(cex=cxtl),zlab=list(cex=cxtl)
                 drape = TRUE,screen = list(z = -200, x = -55),main="Layer1+2 vs 3",par.strip.text=list(cex = strptxt), 
                 vp=vp,col.regions=col.l)#,
  
  b <- wireframe(get(metric) ~ (layer1) * (layer2+layer3) | lm, data=gridout,xlab=list("layer1",rot=xrot),ylab=list("layer2+3",rot=yrot),zlab=list(metric,rot=zrot),
                 scales = list( arrows = F,distance=dist),#,axis=list(text=list(cex=2))
                 drape = TRUE,screen = list(z = -200, x = -55),main="Layer1 vs 2+3",par.strip.text=list(cex = strptxt),vp=vp,col.regions=col.l)#,
  c <-   ggplot(gridout,aes(x=complexity,y=get(metric)),group=lm) + geom_line(aes(color=lm),size=0.75)+
                  geom_point(aes(x=gridout$complexity[index_best], y=gridout[index_best,metric]), colour="black",size=3,shape=1)+
                  ylab(metric)+geom_label(x=gridout$complexity[index_best], y=gridout[index_best,metric]-0.05*gridout[index_best,metric], label=structure)+
                  ylim(c(min(gridout[,metric])-0.1*min(gridout[,metric]),max(gridout[,metric])))
  
  #for label: get the model structure that is dfined as best
  
  grid.arrange(arrangeGrob(a,b,c,widths = c(0.2,1, 1,0.2),
                           heights =c(4,1),
                           layout_matrix = rbind(c(1,1,2,2),
                                                 c(NA,3, 3,NA)),
                           top=textGrob(paste0(region,": ",name," ",structure), gp=gpar(fontsize=20,font=1))))#x = unit(0.02, "npc")
  # grid.arrange(a,b,ncol=2)
  # grid.arrange(c)
  
}


#########################################
###Select best model from gridout, fit again and create analysis plots
#########################################

funcRefitNN <- function(region,name,SorW,metric="Rsq_te",indxno){#metric= Rsq/RMSE te/tr, "SorW"=S/W,indxno=1/2
  #####GET DATASET
  path_tr <- paste0(pathanaldat,"/NON STANDARDIZED/5_Reg",region,"_Tr_",SorW,".csv")
  path_te <- paste0(pathanaldat,"/NON STANDARDIZED/5_Reg",region,"_Te_",SorW,".csv")
  
  inp <- read.csv(path_tr)
  dropvar <- which(names(inp)%in%c("Station","Qmean","Qvol","Area_GIS","Station1","EventID","Event","YYYY","Startdate","Enddate","Duration","Group_ID","Keep","Region","Subregion",
                                   "StrahlerMax","LengthAll","FLSD","PET_Ann","Peff_Ann","Elev_SD","Altitude","Elev_Min","Elev_Max","PmaxT","PminT","P0","P1","P3","P5",
                                   "P7","Pmin","Pmax","PET0", "PET1", "PET3", "PET5","PET7",paste0("AET",c(0,1,3,5,7))))
  data <- inp[,-dropvar]
  
  inp_te <- read.csv(path_te)
  dropvar_te <- which(names(inp_te)%in%c("Station","Qmean","Qvol","Area_GIS","Station1","EventID","Event","YYYY","Startdate","Enddate","Duration","Group_ID","Keep","Region","Subregion",
                                         "StrahlerMax","LengthAll","FLSD","PET_Ann","Peff_Ann","Elev_SD","Altitude","Elev_Min","Elev_Max","PmaxT","PminT","P0","P1","P3","P5",
                                         "P7","Pmin","Pmax","PET0", "PET1", "PET3", "PET5","PET7",paste0("AET",c(0,1,3,5,7))))
  data_te <- inp_te[,-dropvar_te]
  
  proc <- preProcess(data,method = c("center", "scale"))#standardize training data
  data_norm <- predict(proc,data)
  train_ma <- as.matrix(select(data_norm,-Qmax))#training data as matrix
  data_sd <- sd(data$Qmax)
  data_mean <- mean(data$Qmax)
  data_te_norm <- predict(proc,data_te)#standardize test data
  test_ma <- as.matrix(select(data_te_norm,-Qmax))
  
  
  ###Refit model
  gridout <-  read.csv(paste0(pathanal,"/",region,"/",name,"_HTgrid_Reg",region,".csv"))
  gridout$complexity <-  with(gridout,(39*layer1)+(layer1*layer2)+(layer2*layer3)+layer3+layer1+layer2+layer3)
  gridout$lm <- paste(gridout$lr,gridout$momentum,sep=", ")

  if (grepl("Rsq",metric)){index <- which(gridout[,metric]>max(gridout[,metric])-0.01*max(gridout[,metric]))}#but a buffer of 1% so that almost-as-good models o
  if (grepl("RMSE",metric)){index <- which(gridout[,metric]<min(gridout[,metric])+0.01*min(gridout[,metric]))}#f lower complexity are preferred
  
  if (length(index)==1&indxno==2){#if only single best model, determine second best
    if (grepl("Rsq",metric)){
      index_best <- order(gridout[,metric],decreasing=T)[2]
      print(paste0("Only one top model, the one of second highest ",metric," was selected"))
    }
    if (grepl("RMSE",metric)){
      index_best <- order(gridout[,metric],decreasing=F)[2]
      print(paste0("Only one top model, the one of second lowest ",metric," was selected"))
    }
  }
  
  if (length(index)>1&indxno==1){# find the simplest model of of multiple best
    index_comp <- which.min(gridout$complexity[index])#of best, find lowest in complexity
    index_best <- index[index_comp]
    print(paste0("Multiple top models, the one of lowest complexity was selected"))
  }
  
  if (length(index)>1&indxno==2){# find the second simplest model of of multiple best
    index_comp <- order(gridout$complexity[index])[2]#order all, find second lowest in complexity of these
    index_best <- index[index_comp]
    print(paste0("Multiple top models, the one of second lowest complexity was selected"))
  }
  
  if (length(index)==1&indxno==1){index_best <- index}
  
  structure <- with(gridout[index_best,],paste0("39-",layer1,"-",layer2,"-",layer3," (",lm,")"))
  print(paste0(region," ",SorW,": ",structure))
#########MODEL RECALIBRATION  TO OBTAIN HDF5 object
  use_session_with_seed(43, disable_gpu = FALSE, disable_parallel_cpu = FALSE)#set seed but enable parallel processing#43
  
  model<- keras_model_sequential() %>%  #first hidden layer
    layer_dense(units = gridout$layer1[index_best], activation = "sigmoid", input_shape = 39,kernel_initializer = initializer_glorot_uniform(seed=2))#2
  if (gridout$layer2[index_best]>0) {#add second hidden layer if wanted
    model %>%   layer_dense(units = gridout$layer2[index_best], activation = "sigmoid",kernel_initializer = initializer_glorot_uniform(seed=2))#2
  }
  if (gridout$layer3[index_best]>0) {#add second hidden layer if wanted
    model %>%   layer_dense(units = gridout$layer3[index_best], activation = "sigmoid",kernel_initializer = initializer_glorot_uniform(seed=2))#2
  } 
  model %>%  layer_dense(units = 1, activation = "linear")#output layer
  
  # checkpoint <- callback_model_checkpoint(paste0(pathANN,"/keras_opt"), monitor = "val_loss", verbose = 1,
  # save_best_only = TRUE, save_weights_only = FALSE,
  # mode = c("auto","min", "max"), period = 1)
  
  earlystop <- callback_early_stopping(monitor = "val_loss", min_delta = 0, patience = 40,
                                       verbose = 0, mode = c("auto", "min", "max"))
  
  sgd <- optimizer_sgd(lr = gridout$lr[index_best], momentum = gridout$momentum[index_best], decay = 0, nesterov = FALSE,
                       clipnorm = NULL, clipvalue = NULL)
  
  model %>% compile(loss='mean_squared_error', optimizer='sgd',metrics = c("mean_squared_error"))
  
  fm <- model %>% fit(
    train_ma, data_norm$Qmax, 
    epochs = gridout$n_epoch[index_best],
    batch_size = gridout$batch[index_best],
    validation_split = 0.2,
    view_metrics=FALSE,callbacks = earlystop,
    verbose=0
  )
  #export final model
  save_model_hdf5(model, paste0(pathanal,"/",region,"/fmTEST",name,"_Reg",region,"_",metric,"_",indxno,".h5"))
  
  #create analysis plot
  pred_tr <- model %>% predict(train_ma)*data_sd+data_mean
  pred_te <- model %>% predict(test_ma)*data_sd+data_mean
  
  # pdf(paste0(pathANN,"/",name,"_Reg",region,"_",metric,"_",indxno,".pdf"))
  # par(mfrow=c(2,2))
  # funcRes(pred_tr,data$Qmax,"Tr",add=T)
  # funcRes(pred_te,data_te$Qmax,"Te",add=T)
  # par(mfrow=c(1,1))
  # y_range <- range(c(fm$metrics$mean_squared_error,fm$metrics$val_mean_squared_error))
  # plot(fm$metrics$mean_squared_error,type="l",col="black",lwd=2,xlab="Epochs",ylab="MSE",ylim=y_range)
  # lines(fm$metrics$val_mean_squared_error,col="orange",lwd=2)
  # legend("topright",legend=c("Cal","Val"),col=c("black","orange"),lty=1,lwd=2)
  # dev.off()
}

#########################################
###Give out the best ANN strucutre of each region
#########################################
funbestgrid <- function(region,name,metric,indxno){
  gridout <-  read.csv(paste0(pathanal,"/",region,"/",name,"_HTgrid_Reg",region,".csv"))
  gridout$complexity <-  with(gridout,(39*layer1)+(layer1*layer2)+(layer2*layer3)+layer3+layer1+layer2+layer3)
  gridout$lr <- as.factor(paste0("lr=",gridout$lr))
  gridout$momentum <- as.factor(paste0("m=",gridout$momentum))
  gridout$lm <- paste(gridout$lr,gridout$momentum,sep=", ")
  
  if (grepl("Rsq",metric)){index <- which(gridout[,metric]>max(gridout[,metric])-0.01*max(gridout[,metric]))}#but a buffer of 1% so that almost-as-good models o
  if (grepl("RMSE",metric)){index <- which(gridout[,metric]<min(gridout[,metric])+0.01*min(gridout[,metric]))}#f lower complexity are preferred
  
  if (length(index)==1&indxno==2){#if only single best model, determine second best
    if (grepl("Rsq",metric)){
      index_best <- order(gridout[,metric],decreasing=T)[2]
      print(paste0("Only one top model, the one of second highest ",metric," was selected"))
    }
    if (grepl("RMSE",metric)){
      index_best <- order(gridout[,metric],decreasing=F)[2]
      print(paste0("Only one top model, the one of second lowest ",metric," was selected"))
    }
  }
  if (length(index)>1&indxno==1){# find the simplest model of of multiple best
    index_comp <- which.min(gridout$complexity[index])#of best, find lowest in complexity
    index_best <- index[index_comp]
    print(paste0("Multiple top models, the one of lowest complexity was selected"))
  }
  if (length(index)>1&indxno==2){# find the second simplest model of of multiple best
    index_comp <- order(gridout$complexity[index])[2]#order all, find second lowest in complexity of these
    index_best <- index[index_comp]
    print(paste0("Multiple top models, the one of second lowest complexity was selected"))
  }
  if (length(index)==1&indxno==1){index_best <- index}
  
  structure <- with(gridout[index_best,],paste0("39-",layer1,"-",layer2,"-",layer3," (",lm,")"))
  
  return(c(gridout[index_best,],Region=paste0(region,name),Structure=structure))
}

#########################################
###Merge all hypertuning-grid results
#########################################

funallgrids <- function(region,name){
  gridout <-  read.csv(paste0(pathanal,"/",region,"/",name,"_HTgrid_Reg",region,".csv"))
  gridout$Rsq_mean <- apply(cbind(gridout$Rsq,gridout$Rsq_Te),1,mean)
  gridout$Rsq_mean_s <- (gridout$Rsq_mean-min(gridout$Rsq_mean))/(max(gridout$Rsq_mean)-min(gridout$Rsq_mean))
  gridout$Rsq_Te_s <- (gridout$Rsq_Te-min(gridout$Rsq_Te))/(max(gridout$Rsq_Te)-min(gridout$Rsq_Te))
  gridout$Rsq_s <- (gridout$Rsq-min(gridout$Rsq))/(max(gridout$Rsq)-min(gridout$Rsq))
  gridout$RMSE_te_s <- (gridout$RMSE_te-min(gridout$RMSE_te))/(max(gridout$RMSE_te)-min(gridout$RMSE_te))
  
  return(cbind(gridout,Region=paste0(name,region)))
  # index_best <- which.max(gridout$Rsq_Te+gridout$Rsq)
  # print(paste0(region,name))
  # print(gridout[index_best,])
}

#########################################
###Derive Olden and Permuted VarImps and save as file
#########################################

# funcVINN <- function(region,name,SorW,n_br=50,metric,indxno){
#   #####GET DATASET
#   path_tr <- paste0(pathanaldat,"/NON STANDARDIZED/5_Reg",region,"_Tr_",SorW,".csv")
# 
#   inp <- read.csv(path_tr)
#   dropvar <- which(names(inp)%in%c("Station","Qmean","Qvol","Area_GIS","Station1","EventID","Event","YYYY","Startdate","Enddate","Duration","Group_ID","Keep","Region","Subregion",
#                                    "StrahlerMax","LengthAll","FLSD","PET_Ann","Peff_Ann","Elev_SD","Altitude","Elev_Min","Elev_Max","PmaxT","PminT","P0","P1","P3","P5",
#                                    "P7","Pmin","Pmax","PET0", "PET1", "PET3", "PET5","PET7",paste0("AET",c(0,1,3,5,7))))
#   data <- inp[,-dropvar]
#   
#   proc <- preProcess(data,method = c("center", "scale"))#standardize training data
#   data_norm <- predict(proc,data)
#   train_ma <- as.matrix(select(data_norm,-Qmax))#training data as matrix
# 
#   ###########DERIVE VARIMPs
#   model <- load_model_hdf5(paste0(pathanal,"/",region,"/fm",name,"_Reg",region,"_",metric,"_",indxno,".h5"))
#   ###Permuted
#   Imps_B <- breiman(model,data_norm$Qmax,train_ma,n_br)
#   Imps_B <- data.frame(importance=Imps_B,var=names(Imps_B))
#   saveRDS(Imps_B,paste0(pathanal,"/",region,"/",name,"_VarImps_B_",region,"_",metric,"_",indxno,".R"))
#   #ggplot(Imps_B, aes(x=reorder(var,importance),y=importance)) + geom_bar(stat="identity")+
#   #theme(axis.text.x = element_text(angle = 90, hjust = 1))
#   
#   
#   ###Olden aka Connection Weights
#   wK <- get_weights(model)
#   kk <- funcKerastoNN(wK)#restructure weights so that they work with NeuralNetTool
#   Imps_O <- olden(kk$wts,bar_plot=F,x_names = colnames(train_ma),y_names="Qmax")
#   Imps_O <- data.frame(importance=Imps_O,var=row.names(Imps_O))
#   
#   saveRDS(Imps_O,paste0(pathanal,"/",region,"/",name,"_VarImps_O_",region,"_",metric,"_",indxno,".R"))
# }



#########################################
# Create Analysis pots only
#########################################

#Not ready to run! HIER FEHLT SorW!

funcAnalPlotNN <- function(region,name,main,pdf=F){
  path_tr <- paste0(pathanaldat,"/NON STANDARDIZED/5_Reg",region,"_Tr_S.csv")
  path_te <- paste0(pathanaldat,"/NON STANDARDIZED/5_Reg",region,"_Te_S.csv")
  model <-   load_model_hdf5(paste0(pathanal,"/",region,"/fm",name,"_Reg",region,".h5"))
  inp <- read.csv(path_tr)
  dropvar <- which(names(inp)%in%c("Station","Qmean","Qvol","Area_GIS","Station1","EventID","Event","YYYY","Startdate","Enddate","Duration","Group_ID","Keep","Region","Subregion",
                                   "StrahlerMax","LengthAll","FLSD","PET_Ann","Peff_Ann","Elev_SD","Altitude","Elev_Min","Elev_Max","PmaxT","PminT","P0","P1","P3","P5",
                                   "P7","Pmin","Pmax","PET0", "PET1", "PET3", "PET5","PET7",paste0("AET",c(0,1,3,5,7))))
  data <- inp[,-dropvar]

  inp_te <- read.csv(path_te)
  dropvar_te <- which(names(inp_te)%in%c("Station","Qmean","Qvol","Area_GIS","Station1","EventID","Event","YYYY","Startdate","Enddate","Duration","Group_ID","Keep","Region","Subregion",
                                         "StrahlerMax","LengthAll","FLSD","PET_Ann","Peff_Ann","Elev_SD","Altitude","Elev_Min","Elev_Max","PmaxT","PminT","P0","P1","P3","P5",
                                         "P7","Pmin","Pmax","PET0", "PET1", "PET3", "PET5","PET7",paste0("AET",c(0,1,3,5,7))))
  data_te <- inp_te[,-dropvar_te]

  proc <- preProcess(data,method = c("center", "scale"))#standardize training data
  data_norm <- predict(proc,data)
  train_ma <- as.matrix(select(data_norm,-Qmax))#training data as matrix
  data_sd <- sd(data$Qmax)
  data_mean <- mean(data$Qmax)
  data_te_norm <- predict(proc,data_te)#standardize test data
  test_ma <- as.matrix(select(data_te_norm,-Qmax))


  pred_tr <- model %>% predict(train_ma)*data_sd+data_mean
  pred_te <- model %>% predict(test_ma)*data_sd+data_mean

  if (pdf){pdf(paste0(pathANN,"/",name,"_Reg",region,"_recal.pdf"))}
  par(mfrow=c(2,2))
  funcRes(pred_tr,data$Qmax,"Tr",add=T)
  mtext(paste0(region, ": ",main),3,0,outer=T,font=2)
  funcRes(pred_te,data_te$Qmax,"Te",add=T)
  # par(mfrow=c(1,1))
  # y_range <- range(c(fm$metrics$mean_squared_error,fm$metrics$val_mean_squared_error))
  # plot(fm$metrics$mean_squared_error,type="l",col="black",lwd=2,xlab="Epochs",ylab="MSE",ylim=y_range)
  # lines(fm$metrics$val_mean_squared_error,col="orange",lwd=2)
  # legend("topright",legend=c("Cal","Val"),col=c("black","orange"),lty=1,lwd=2)
  if (pdf) {dev.off()}
}


###########FUNCTIONS
breiman <- function (model, y, covdata, nrep){
  #model is an object of class nnet as returned by nnet
  #y is a vector containing the target (or dependent variable)
  #covdata is a data frame containing the covariates
  #nrep specifies the number of Monte Carlo simulations for computing the
  # variable importance
  
  #calculate mean squared error of model (will be used as benchmark)
  residuals <- y-predict(model, covdata)
  MSE <- mean(residuals^2)
  
  #compute variable importance
  ncov <- ncol(covdata)
  nobs <- nrow(covdata)
  
  vi <- sapply(1:ncov, function (i){
    simMat <- covdata[rep(seq(nobs), nrep), ]# matrix of input data concennated to each other n rep times (for prediction-/simulation matrix)
    permutedCol <- as.vector(replicate(nrep, sample(covdata[,i], nobs, replace=FALSE)))# shuffle the respective predictor column nrep times
    indicatorNrep <- rep(1:nrep, each=nobs)#a vector of nrep indexes, indicating which nrep each permuted value belongs to
    simMat[,i] <- permutedCol# fill into simulation matrix
    permuted.pred <- predict(model, simMat)#predict from these concenated, permuted datasets
    SMSE <- tapply(permuted.pred, indicatorNrep, function (j) mean((y-j)^2))#apply by facor, i.e. nrep: For each nrep separately, calculate difference from original y
    mean((SMSE-MSE)/MSE)})#Difference in accuracy scaled to original error
  
  names(vi) <- colnames(covdata)
  return(vi)
}


funcKerastoNN <- function(wK){
  ###restructuring keras weights to use NeuralNetTools
  wt <- list()
  wt$struct <- NROW(wK[[1]])#no.of inputs
  #indexes using all layers
  index_all <- seq(1,length(wK)/2,1)#how many layers?
  index_all2 <- c(1,1+2*(index_all[-length(index_all)]))#indexes of weight list items(excluding bias items)
  
  for (i in index_all2){
    wt$struct <- c(wt$struc,NCOL(wK[[i]]))
  }
  #first hidden layer
  for (h in index_all){#layer iteration
    index_weight <- index_all2[h]
    index_bias <- index_all2[h]+1
    for (i in 1:NCOL(wK[[index_weight]])){#node iteration
      name <- paste("hidden",h,i,sep=" ")
      # print(name)
      # cat("weight:",index_weight,"\n")
      # cat("bias:",index_bias,"\n")
      wt$wts[[name]] <- as.numeric(c(wK[[index_bias]][i],wK[[index_weight]][,i]))
    }
  }
  #rename output layer
  names(wt$wts)[length(wt$wts)] <- "out 1"
  return(wt)
}



