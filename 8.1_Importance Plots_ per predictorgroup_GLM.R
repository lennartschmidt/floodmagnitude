# install.packages("caret")
# install.packages("recipes")
library("randomForest")
library(caret)
library("ggplot2")
library(dplyr)
library(gridExtra)
library("grid")
library(reshape2)

#####################PLOTS PER PREDICTOR GROUP################
VarImps <- readRDS(paste0(pathanaldat,"/8_Coeffs_SumsSC_","GLM2_PCAraw",".R")) 
pdf(paste0(pathanal,"/0 All/8_Importances_All_GLM2_by_pred.pdf"),paper="a4r")
plotvars <- c("SM","Peff","T","Statics")
labels <- c("Soil Moisture", "Precipitation","Temperature","Static Variables")
vars_drop <- sapply(as.list(paste0("^",plotvars[1:3])),FUN=grep,x=VarImps$Var, value=F)#filter all that START with T,Sm etc
vars_drop <- unlist(vars_drop)
VarImps$Var <- as.character(VarImps$Var)
VarImps$Var[-vars_drop] <- c("Area", "Altitude", "Forest", "Impervious", "Permeable", "AI", 
                             "P_Ann", "Slope", "DD", "ChSlope", "FLCV", "FLMax")#makes names appropriate
# vars_drop <- vars_drop[-which(VarImps$Var[vars_drop]=="P_Ann")]#move P_ann from peff set to static variables

for (plotvar in plotvars){  
  plotlist <- list()
  ylmax <- list()
  for (a in 1:4){
    #yl_dyn <- c(0,max(select(VarImps[vars_drop,],c(paste0("S",a),paste0("W",a))),na.rm=T))#same y-scaling for all plots
    #yl_stat <- c(0,max(select(VarImps[-vars_drop,],c(paste0("S",a),paste0("W",a))),na.rm=T))#same y-scaling for all plots
    ###SM
    if (plotvar=="Statics") {#dfferent scaling of plots and labels for static predictors
      VarImps2 <-VarImps[-vars_drop,c("Var",paste0("S",a),paste0("W",a))]
      #VarImps2 <- rbind(VarImps2,VarImps[which(VarImps$Var=="Peff_Ann"),c("Var",paste0("S",a),paste0("W",a))])#add Peff to statics
      labelrot <- theme(axis.text.x = element_text(angle = 90,vjust = 0.5,hjust = 0))#axis label rotation and position
    }else{
      vars <- grep(paste0("^",plotvar), VarImps$Var, value=F)#filter the one in question that STARTS with T,Sm etc
      VarImps2 <-VarImps[vars,c("Var",paste0("S",a),paste0("W",a))]
      # if(plotvar=="P"){VarImps2 <- VarImps2[-which(VarImps2$Var=="P_Ann"),]}#delete Peff_Ann this set as it is included in statics      
      VarImps2$Var <- gsub(plotvar, "", VarImps2$Var)#change names for easy to read labels
      VarImps2$Var[which(VarImps2$Var%in%c("max","min"))] <- c("Max","Min")
      labelrot <- theme(axis.text.x = element_text(angle = 0))  #axis label rotation and position
    }
    names(VarImps2) <- c("Variable","Summer","Winter")
    narows <- which(rowMeans(is.na(VarImps2[,c("Summer","Winter")])) > 0.5)#keep only rows that contain at least on value
    if(length(narows)>0){VarImps2 <- VarImps2[-narows,]}# gives error if 0
    #VarImps2[,c(2,3)] <-  VarImps2[,c(2,3)]/sum(VarImps2[,c(2,3)],na.rm=T)*100
    VarImps2 <- melt(VarImps2,id.vars = 1)
    # Grouped
    bb <- ggplot(VarImps2, aes(y=value, x=Variable)) + 
      geom_bar(aes(fill = variable),position="dodge", stat="identity")+
      scale_fill_manual("Season", values = c("Summer" = "darkorange", "Winter" = "blue"))+
      labelrot+
      labs(title=a,x="",y="")+#y="MSE Increase [%]",
      theme(plot.title = element_text(hjust=0.5))
    plotlist[[a]] <- bb#save plot
    ylmax <- append(ylmax,range(VarImps2$value,na.rm=T))#save range of yvalue for scaling
  }
  
  
  
  
  # grid.arrange(
  #   grobs = plotlist,
  #   widths = c(1, 1),
  #   top=textGrob(a,gp=gpar(fontsize=20,font=1)),
  #   layout_matrix = rbind(c(1, 2),
  #                         c(3, 4))
  # )
  
  g_legend<-function(a.gplot){# function to extract legend for common legend of plots
    tmp <- ggplot_gtable(ggplot_build(a.gplot))
    leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
    legend <- tmp$grobs[[leg]]
    return(legend)}
  
  mylegend<-g_legend(plotlist[[1]])
  yl <- c(range(unlist(ylmax)))#same axis scaling
  grid.arrange(arrangeGrob(plotlist[[1]] + theme(legend.position="none")+ylim(yl),
                           plotlist[[2]] + theme(legend.position="none")+ylim(yl),
                           plotlist[[3]] + theme(legend.position="none")+ylim(yl),
                           plotlist[[4]] + theme(legend.position="none")+ylim(yl),
                           mylegend,
                           widths = c(1, 1),
                           heights =c(0.3,1,1),
                           layout_matrix = rbind(c(5,5),
                                                 c(1, 2),
                                                 c(3, 4)
                           )))
  lab <- labels[which(plotvars==plotvar)]#get decent label name
  text <- bquote((-~delta~t~of)~.(lab))
  if(lab=="Static Variables"){text <- lab}
  grid.text(text, just="center", x = unit(0.52, "npc"),
            y = unit(0.015, "npc"))
  grid.text("Relative Coefficient [%]", just="left", x = unit(0.017, "npc"), 
            y = unit(0.33, "npc"),rot=90)
}
dev.off()

# maxheight = unit.pmax(plotlist[[1]]$widths, p2$widths[2:3])
# for (k in 1:4){
#   plotlist[[1]] <- ggplot_gtable(ggplot_build(plotlist[[1]] + theme(legend.position="none")))
# }


