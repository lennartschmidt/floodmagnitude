import arcpy
from arcpy import env
from arcpy.sa import *

import os
import shutil
#from osgeo import gdal
import numpy as np
import pandas as pd


#set arcpy global variables
arcpy.CheckOutExtension("Spatial") #get spatial analyst extention
arcpy.env.overwriteOutput = True
#arcpy.env.scratchFolder = "C:/Users/schmidle/Desktop/Scratch"

# filepaths
#office
#path = "C:/Users/schmidle/Desktop/Masterarbeit/RawData/morphdata/"#Input
#path2 = "C:/Users/schmidle/Desktop/Masterarbeit/Analysis/Data/Geodata/Predictors/"#Output
#server
path = "C:/Users/schmidle/Desktop/morphdata/"#Input
path2 = "C:/Users/schmidle/Desktop/Predictors/"#Output


# lists for output
Station = []
StrahlerMax = []
Area = []
LengthAll = []
DD = []
ChLength = []
ChSlope = []
Slope = []
FLSD = []
FLCV = []
FLMax = []
Errors = ["WARNINGS: "]
direct =  filter(lambda x: os.path.isdir(os.path.join(path, x)), os.listdir(path))#folder list for
#iteration, isdir and .join to exlude non-folders
count =1
for i in direct:
    catch_id = i.translate(None, 'sub_')#catchment ID
    Station.append(catch_id)#for output table
    print("BASIN: "+catch_id+" , "+str(count)+" of "+str(len(direct)))
    path_in = path+i+"/" #paths for each subfolder per basin
    print("IN: "+str(path_in))
    arcpy.env.workspace = path_in
    path_out = path2+i+"/"
    print("OUT: "+str(path_out))
    if not os.path.exists(path_out):#create subfolders
        os.makedirs(path_out)
        
##    #clear subfolder to avoid problems with arcpy overwriting that doesnt always work
##    for name in os.listdir(path_out):
##        p = os.path.join(path_out, name)
##        try:
##            if os.path.isdir(p):
##                shutil.rmtree(p)
##            else:
##                os.remove(p)
##        except shutil.Error as e:
##            print ('Ein Fehler ist aufgetreten: ', e)

    ##### Calculate Slope of all Rivers (3 highest orders) #####
    slope = "slope.asc"
    streamorder_cut =  path_out+"StreamOrder_cut.shp"
    #arcpy.DefineProjection_management(slope, sr)
    slope_mainstream = ExtractByMask (slope, mainstream)
#    slope_mainstream_m =  arcpy.GetRasterProperties_management(slope_mainstream, "MEAN").getOutput(0).replace(",", ".")#replace decimal symbol
    slope_mainstream_m = slope_mainstream.mean
    ChSlope.append(slope_mainstream_m)

    mainstream = None
    slope_mainstream = None


    ##### Calculate Catchment Slope #####
    slope = arcpy.CalculateStatistics_management(slope)
    slope_catchment = arcpy.GetRasterProperties_management(slope, "MEAN").getOutput(0).replace(",", ".")
    Slope.append(slope_catchment)

    slope = None


    count = count+1
    #clean all variables/raster layers (otherwise arcpy scratch rasters persist)
    #path_in = None
    #path_out = None
    #pathout2 = None
    #slope_mainstream_m = None
    #slope_catchment = None



    
# Create Output table
output = pd.DataFrame(np.column_stack([Station, Slope, StrahlerMax, LengthAll, Area, DD, ChLength, ChSlope, FLSD, FLCV, FLMax]),
                      columns=["Station", "Slope", "StrahlerMax", "LengthAll", "Area", "DD", "ChLength", "ChSlope", "FLSD", "FLCV", "FLMax"])
#print(output)
output.to_csv(path2+"Geoprocess_Preds.csv",index=False, sep=";")
print(Errors)
#output.to_csv("C:/Users/schmidle/Desktop/Geoprocess_Preds.csv",index=False, sep=";")
# Check out the ArcGIS Spatial Analyst extension license SUM_<field>
arcpy.CheckOutExtension("Spatial") 
