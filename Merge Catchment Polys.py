import arcpy
from arcpy import env
from arcpy.sa import *
import os

path1 = "C:/Users/schmidle/Desktop/Masterarbeit/Geodata/Predictors/"#Input
path2 = "C:/Users/schmidle/Desktop/Masterarbeit/Geodata/All_Catchments.shp"

direct =  filter(lambda x: os.path.isdir(os.path.join(path1, x)), os.listdir(path1))#folder list for
polys = [path1+s+"/CatchPoly.shp" for s in direct]

print(polys)

##path_in = path1+direct[0]+"/"
##All_Catch = path_in+"CatchPoly.shp"
##
##for i in direct[1:3]:
##    path_in = path1+i+"/"+"CatchPoly.shp" #paths for each subfolder per basin
##    print("IN: "+str(path_in))
##
arcpy.Merge_management(polys, path2)
