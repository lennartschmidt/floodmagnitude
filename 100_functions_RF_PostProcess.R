################################################
#RF Model Analysis Functions
################################################

#################################
#GIVE OUT MODEL SIZES
################################
funcRFsize <- function(name){
  sizes <- data.frame(Model=paste0(c("S","W"),rep(1:4,each=2)),Size=NA)  
  #sizes=list()
  for (a in 1:4){
  rf1 <- readRDS(paste0(pathanal,"/",a,"/fm",name,"_S_Reg",a,".R"))$fit
  rf2 <- readRDS(paste0(pathanal,"/",a,"/fm",name,"_W_Reg",a,".R"))$fit
  # sizes <- rbind(sizes,c(paste0("S",a),nrow(rf1$importance)))
  # sizes <- rbind(sizes,c(paste0("W",a),nrow(rf2$importance)))
  sizes[sizes$Model==paste0("S",a),"Size"] <- nrow(rf1$importance)
  sizes[sizes$Model==paste0("W",a),"Size"] <- nrow(rf2$importance)
  }
  return(sizes)
}

######################################################################
#OLD: SUM VARIABLE IMPORTANCES OF ALL MODELS
######################################################################
funcVIRFMerge <- function(model){
  VarImps <- data.frame("Var"=names(data))
  for (a in 1:4){
    fms <- readRDS(paste0(pathanal,"/",a,"/fmrfPeff3_r_S_Reg",a,".R"))$fit
    fmw <- readRDS(paste0(pathanal,"/",a,"/fmrfPeff3_r_W_Reg",a,".R"))$fit
    # fms <- readRDS(paste0(pathanal,"/",a,"/fmrfPeff_S_Reg",a,".R"))
    # fmw <- readRDS(paste0(pathanal,"/",a,"/fmrfPeff_W_Reg",a,".R"))
    
    
    imp_s <- unlist(fms$importance[,1])#merge summer importances into dataframe
    ifelse(length(imp_s)>=15,N<-15,N<-length(imp_s))#limit to 27 highest-ranked variables
    imp_s <- sort(imp_s, decreasing=TRUE)[1:N]
    imp_s <- imp_s/sum(imp_s)*100#normalize to sum
    imp_s <- data.frame(I=imp_s,Var=names(imp_s),row.names = NULL)
    names(imp_s) <- c(paste0("S",a),"Var")
    VarImps <- merge(VarImps,imp_s,by="Var",all=TRUE)
    imp_w <- unlist(fmw$importance[,1])#winter
    ifelse(length(imp_w)>=15,N2<-15,N2<-length(imp_w))#limit to 27 highest-ranked variables
    imp_w <- sort(imp_w, decreasing=TRUE)[1:N2]
    imp_w <- imp_w/sum(imp_w)*100#normalize to sum
    imp_w <- data.frame(I=imp_w,Var=names(imp_w),row.names = NULL)
    names(imp_w) <- c(paste0("W",a),"Var")
    VarImps <- merge(VarImps,imp_w,by="Var",all=TRUE)
  }
  saveRDS(VarImps,paste0(pathanaldat,"/8_VarImps_RFPeff4",".R"))  
}

######################################
###AVERAGE VARIABLE IMPORTANCES: WHOLE REGION+BY PREDICTOR
#######################################


funcVIRFMeans <- function(model){
  # AI and Pann are excluded before
  VarImps <- readRDS(paste0(pathanal,"/0 All/8_VarImps_",model,".R"))
  
  
  ### AVERAGING VARIABLE IMPORTANCES
  plotvars <- c("SM","Peff","T","Statics")
  labels <- c("Soil Moisture", "Precipitation","Temperature","Static Variables")
  vars_drop <- sapply(as.list(paste0("^",plotvars[1:3])),FUN=grep,x=VarImps$Var, value=F)#filter all that START with T,Sm etc
  vars_drop <- unlist(vars_drop)
  VarImps$Var <- as.character(VarImps$Var)
  
  ######OVERALL AVERAGES BY SEASON, EXCLUDING AI AND PANN
  out0 <- data.frame(Season=c("Summer","Winter"))
  out0[,plotvars] <- NA
  out0_sd <- out0
  for (plotvar in plotvars){  
    if (plotvar=="Statics") {#dfferent scaling of plots and labels for static predictors
      VarImps2 <-VarImps[-vars_drop,]
      VarImps2 <- VarImps2[-which(VarImps2$Var%in%c("AI_Ann","P_Ann")),]
    }else{
      vars <- grep(paste0("^",plotvar), VarImps$Var, value=F)#filter the one in question that STARTS with T,Sm etc
      VarImps2 <-VarImps[vars,]
      #VarImps2$Var <- gsub(plotvar, "", VarImps2$Var)#change names for easy to read labels
    }
    #names(VarImps2) <- c("Variable","Summer","Winter")
    # narows <- which(rowMeans(is.na(VarImps2[,c("Summer","Winter")])) > 0.5)#keep only rows that contain at least one value
    # if(length(narows)>0){VarImps2 <- VarImps2[-narows,]}# gives error if 0
    
    indexvar <- which(names(out0)==plotvar)#variable column in output df
    VarImps2 <- as.matrix(select(VarImps2,-Var))
    out0[1,indexvar] <- mean(VarImps2[,c(paste0("S",1:4))],na.rm=T)#Summer
    out0[2,indexvar] <- mean(VarImps2[,c(paste0("W",1:4))],na.rm=T)#Winter
    out0_sd[1,indexvar] <- sd(VarImps2[,c(paste0("S",1:4))],na.rm=T)#S
    out0_sd[2,indexvar] <- sd(VarImps2[,c(paste0("W",1:4))],na.rm=T)#W
  }
  
  # Plot
  # out%>%select(-Region)%>%group_by(Season)%>%summarize_all(mean,na.rm=T)->VImeans
  # out%>%select(-Region)%>%group_by(Season)%>%summarize_all(sd,na.rm=T)->VIsd
  VImeans <- melt(out0)#restrucutre and merge mean and sd for plotting
  VIsd <- melt(out0_sd,value.name = "sd")
  VImeans <- left_join(VImeans,VIsd)
  
  VImeans$variable <- factor(VImeans$variable,levels=c("Peff","SM","T","Statics"))
  # pdf(paste0(pathanal,"/0 All/8_Importances_Means_",model,"_",BorO,".pdf"),onefile=F)
  # png(paste0(pathanal,"/0 All/8_Importances_Means_RFPeff3.png"),res=288)
  plotlist <- list()
  plotlist[[1]] <- ggplot(VImeans, aes(y=value, x=variable,group=Season)) + 
    geom_bar(aes(fill = Season),position="dodge", stat="identity")+
    scale_fill_manual("Season", values = c("Summer" = "darkorange", "Winter" = "blue"))+
    geom_errorbar(aes(ymin = (value - sd), ymax = (value + sd)),position=position_dodge(0.95),width=0.3,size=0.7)+
    labs(x="",y="Mean Relative Importance [%]",title="")+#paste0(model,"_",BorO)
    theme(text =  element_text(size=10), axis.text.x = element_text(size=10))+
    scale_x_discrete(labels = c(expression("P"["eff"]),"SM", "T", "Static"))
  # ggsave(paste0(pathMA,"/VarImpMeans_noAIPann.pdf"),width=8,height=8,unit="cm")
  # }
  # dev.off()  
  
  ###AVERAGES PER REGION AND SEASON, EXCLUDING AI and PANN
  out <- data.frame(Region=c(1,1,2,2,3,3,4,4),Season=rep(c("Summer","Winter"),4))
  out[,plotvars] <- NA
  out_sd <- out
  # VarImps$Var[-vars_drop] <- c("AI","Area","ChSlope","DD","Altitude","FLCV","FLMax","Forest","Impervious","P_Ann","Permeable","Slope")#makes names appropriate
  # vars_drop <- vars_drop[-which(VarImps$Var[vars_drop]=="P_Ann")]#move P_ann from peff set to static variables
  
  for (plotvar in plotvars){  
    for (a in 1:4){
      if (plotvar=="Statics") {#dfferent scaling of plots and labels for static predictors
        VarImps2 <-VarImps[-vars_drop,c("Var",paste0("S",a),paste0("W",a))]
        VarImps2 <- VarImps2[-which(VarImps2$Var%in%c("AI_Ann","P_Ann")),]
      }else{
        vars <- grep(paste0("^",plotvar), VarImps$Var, value=F)#filter the one in question that STARTS with T,Sm etc
        VarImps2 <-VarImps[vars,c("Var",paste0("S",a),paste0("W",a))]
        VarImps2$Var <- gsub(plotvar, "", VarImps2$Var)#change names for easy to read labels
      }
      names(VarImps2) <- c("Variable","Summer","Winter")
      narows <- which(rowMeans(is.na(VarImps2[,c("Summer","Winter")])) > 0.5)#keep only rows that contain at least one value
      if(length(narows)>0){VarImps2 <- VarImps2[-narows,]}# gives error if 0
      
      indexvar <- which(names(out)==plotvar)#variable column in output df
      indexs <- which(out$Region==a)#rows of correct region
      out[indexs[1],indexvar] <- mean(VarImps2$Summer,na.rm=T)
      out[indexs[2],indexvar] <- mean(VarImps2$Winter,na.rm=T)
      out_sd[indexs[1],indexvar] <- sd(VarImps2$Summer,na.rm=T)
      out_sd[indexs[2],indexvar] <- sd(VarImps2$Winter,na.rm=T)
    }
  }
  #saveRDS(out,paste0(pathanal,"/0 All/8_VarImps_",model,"_",BorO,"_",metric,"_",indxno,"_Means_noAIPann.R"))
  
  #Plots
  labels <- c("Effective Precipitation","Soil Moisture", "Temperature","Static Variables")
  plotvars <- c("Peff","SM","T","Statics")
  ylmax <- list()
  ylmin <- list()
  for (i in 1:length(plotvars)){
    plotvar <- plotvars[i]
    plotdf <- cbind(out[,c("Region","Season",plotvar)],sdmin=out[,plotvar]-out_sd[,plotvar],sdmax =out[,plotvar]+out_sd[,plotvar])
    plotlist[[i+1]] <- 
      ggplot(plotdf, aes(x=Region,group=Season)) + aes_string(y=plotvar)+
      geom_bar(aes(fill = Season),position="dodge", stat="identity")+
      geom_errorbar(aes(ymin = sdmin, ymax = sdmax), position=position_dodge(0.95),width=0.3,size=0.7)+
      scale_fill_manual("Season", values = c("Summer" = "darkorange", "Winter" = "blue"))+
      # labelrot+
      #labs(title=labels[i],x="Region",y="Mean Relative Importance [%]")+
      labs(title=labels[i],x="",y="")+
      theme(plot.title = element_text(hjust=0.5), text =  element_text(size=11), axis.text.x = element_text(size=11))
    ylmin <- append(ylmin,min(plotdf$sdmin,na.rm = T))
    ylmax <- append(ylmax,max(plotdf$sdmax,na.rm = T))
    # ggsave(paste0(pathMA,"/VarImp_",plotvar,".png"),width=7.47,height=7.47)
  }
  # pdf(paste0(pathanal,"/0 All/8_Importances_Means_bR_",model,"_",BorO,".pdf"),width=12,height=6,onefile=F)
  g_legend<-function(a.gplot){# function to extract legend for common legend of plots
    tmp <- ggplot_gtable(ggplot_build(a.gplot))
    leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
    legend <- tmp$grobs[[leg]]
    return(legend)}
  
  mylegend<-g_legend(plotlist[[1]])
  ymin <- ifelse(min(unlist(ylmin))<0, min(unlist(ylmin)),0)#if error bars extend below zero, extend yrange
  yl <- c(ymin,max(unlist(ylmax)))#same axis scaling
  grid.arrange(arrangeGrob(plotlist[[1]] + theme(legend.position="none"),
                           plotlist[[2]] + theme(legend.position="none")+ylim(yl),
                           plotlist[[3]] + theme(legend.position="none")+ylim(yl),
                           plotlist[[4]] + theme(legend.position="none")+ylim(yl),
                           plotlist[[5]] + theme(legend.position="none")+ylim(yl),
                           mylegend,
                           widths = c(1,1,1,1),
                           heights =c(1,0.2,1),
                           layout_matrix = rbind(c(NA,1,1,NA),
                                                 c(6,6,6,6),
                                                 c(2,3,4,5)
                           ),
                           top=paste0(model,"_",BorO)))
  # lab <- labels[which(plotvars==plotvar)]#get decent label name
  # text <- bquote((-~delta~t~of)~.(lab))
  # if(lab=="Static Variables"){text <- lab}
  grid.text("Region", just="center", x = unit(0.52, "npc"),
            y = unit(0.023, "npc"),gp=gpar(fontsize=16))
  grid.text("Mean Relative Importance [%]", just="left", x = unit(0.012, "npc"), 
            y = unit(0.18, "npc"),rot=90,gp=gpar(fontsize=13))#
  #grid.text(paste0(model,"_",BorO),)
  # ggsave(paste0(pathMA,"/VarImp_","all",".pdf"),width = 15,height = 7.5)
  # dev.off()
}


